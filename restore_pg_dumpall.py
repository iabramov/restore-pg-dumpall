# Splitting: 
# python restore.py split db_2019-08-14-02-20-01.sql
# Restoring:
# python restore.py restore --host=localhost -utest -ptest db_2019-08-14-02-20-01-dev_migom_dev.sql

import argparse
import os
from collections import deque
import re
from pathlib import PurePath

import gzip
import shutil

# TODO rename database 
# update from memory, without splitting 
# with open('temp.file') as input:
#     out = subprocess.check_output(['cat'], stdin=input)

def cmd_split(args):

    filename = args.filename
    splitted_path = PurePath(filename)

    if '.gz' == splitted_path.suffix:
        with gzip.open(args.filename, 'rb') as f_in:
            with open(splitted_path.stem, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        filename = splitted_path.stem

    base_file_name = PurePath(filename).stem
    regex_dbname = re.compile("^CREATE DATABASE\s+([^\s]*)\s+.*")
    dbname = ""
    dq = deque()
    f_out = None

    with open(filename, "r") as f_in:

        for line in f_in:

            # заголовок пройден, но имя базы нам неизвестно
            if dq :
                dq.append(line)
            elif f_out != None:
                f_out.write(line)
             
            if line.find("-- PostgreSQL database dump",0)==0:
                dq.append(line)
                
            if line.find("CREATE DATABASE",0)==0:

                # determine a database name
                result = regex_dbname.search(line)
                dbname = result.groups()[0]

                if args.name != None and args.name != dbname :
                    # skiping database
                    dq.clear()
                    continue

                f_out = open(base_file_name+"_"+dbname+".sql","w")
                
                # flush queue
                while dq :
                    f_out.write(dq.popleft())

            if line.find("-- PostgreSQL database dump complete",0)==0 :
                dq.clear()
                if f_out != None :
                  f_out.close()
                  f_out = None
        
        if f_out != None :
             f_out.close()

def cmd_restore(args):
    os.system("PGPASSWORD='%s' psql --username=%s --host=%s --port=%s -f %s" % (args.password,args.user,args.host,args.port,args.filename))

def cmd_restore_inmemory(args):
    # TODO rename database 
    # update from memory, without splitting 
    with open('temp.file') as input:
        out = subprocess.check_output(['cat'], stdin=input)

parser = argparse.ArgumentParser(description="Restore database from pg_dumpall file.", usage='python restore_pg_dumpall.py')
subparsers = parser.add_subparsers()

# args without "-" are values
# create the parser for the "split" command
parser_split = subparsers.add_parser('split')
parser_split.add_argument('-n', '--name', type=str, required=False)
parser_split.add_argument('filename', type=str, help='input file')
parser_split.set_defaults(func=cmd_split)

# create the parser for the "restore" command
parser_restore = subparsers.add_parser('restore')
parser_restore.add_argument('-n', '--name', type=str, required=False)
parser_restore.add_argument('-o', '--host', type=str, required=True)
parser_restore.add_argument('--port', type=str, default="5432")
# parser_restore.add_argument('-d', '--dbname', type=str, required=True)
parser_restore.add_argument('-u', '--user', type=str, required=True)
parser_restore.add_argument('-p', '--password', type=str, required=True)
parser_restore.add_argument('filename', type=str, help='input file')
parser_restore.set_defaults(func=cmd_restore)

args = parser.parse_args()
args.func(args)
